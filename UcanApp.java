
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.io.*;
import java.util.*;
import javax.swing.*;

/**
 *
 * @author Abdul Fatir
 */
public class UcanApp {

    /**
     * @param args the command line arguments
     * First argument has to be path to the input file
     */
    public static void main(String[] args) throws IOException, FileNotFoundException {

        String line = null;
        int number_of_edges,number_of_vertices = 0;
        Graph graph;
        List<Contact> contacts = new ArrayList<>();

        System.out.println("Please enter file name");

        // Scanner in = new Scanner(System.in);
        // String file = in.next();

    	// System.out.println(file);

		// "D:\\Node Projects\\Ucan\\ucan-java\\sample_data.txt"
    	File fr = new File("sample_data.txt");
        FileReader in1 = new FileReader(fr);

        //Using Buffer Reader
        BufferedReader br = new BufferedReader(in1);
		

        //Reading from file
        while( (line = br.readLine()) != null){

            String[] data = line.split(",");

//			System.out.println(data[0]);
//			System.out.println(data[1]);
            number_of_vertices++;

            contacts.add(new Contact(data[0], data[1]));
        }

        graph = new Graph(number_of_vertices);

        // need to improve
        for(int i = 0; i < number_of_vertices; i++) {
                for(int j = 0; j < number_of_vertices; j++){
                        if(i == j)
                                continue;
                        if((contacts.get(i).name.equals(contacts.get(j).name)) || (contacts.get(i).number.equals(contacts.get(j).number))){
                            graph.setEdge(i,j);
                        }
                }
        }


        // display
        System.out.println("Found "+number_of_vertices+" contacts");
        System.out.println("Merged Set");


        for(int i = 0; i < number_of_vertices; i++)
        {
                System.out.print(i+":["+contacts.get(i).toString()+"]");

                List<Integer> edgeList = graph.getEdge(i);

                for (int j = 0 ; ; j++ )
                {
                        if (j != edgeList.size())
                        {
                            System.out.print("["+contacts.get(j).toString()+"]");
                            //System.out.print(edgeList.get(j)+"->");
                        }else
                        {
                            System.out.println("["+contacts.get(j).toString()+"]");
                            //System.out.print(edgeList.get(j));
                        break;
                        }						 
                }

                System.out.println();
        }

    }
    
}
