/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Abdul Fatir
 */
public class Contact {
	public String name;
	public String number;

	public Contact(String name, String number) {
		this.name = name;
		this.number = number;
	}
        
        public String toString() {
            return name+", "+number;
        }
}
