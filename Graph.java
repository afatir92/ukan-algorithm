/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Abdul Fatir
 */
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class Graph
{
    private  Map<Integer, List<Integer>> AdjList;


    public Graph(int number_of_vertices)
    {
        AdjList = new HashMap<Integer, List<Integer>>();

        for (int i = 0 ; i <= number_of_vertices ; i++)
        { 
            AdjList.put(i, new LinkedList<Integer>());

        }
    }

    public void setEdge(int source , int destination)
    {
        if (source > AdjList.size() || destination > AdjList.size())
        {
           System.out.println("the vertex entered in not present ");
           return;
        }
        List<Integer> slist = AdjList.get(source);
        slist.add(destination);
        List<Integer> dlist = AdjList.get(destination);
        dlist.add(source);
   }

   public List<Integer> getEdge(int source)
   {
            if (source > AdjList.size())
        {
            System.out.println("the vertex entered is not present");
            return null;
        }
        return AdjList.get(source);
   }

}