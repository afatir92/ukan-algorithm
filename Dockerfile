FROM openjdk:8
COPY . ukan-java-app
WORKDIR ukan-java-app
RUN javac Contact.java
RUN javac Graph.java
RUN javac UcanApp.java
CMD ["java", "UcanApp"]